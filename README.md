# README #

This repo will host every lab that is completed in COSC431.

### What is this repository for? ###

* Labs!

### How do I get set up? ###

* Fork this repo
* Add this url as your upstream
	* git remote add upstream https://bitbucket.org/rvalis/labsapp

### Fetching new branches/code ###

* git fetch upstream
* git merge upstream/<some_branch>