package edu.towson.cosc431.labsapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AddSongActivity extends AppCompatActivity {

    public static final String SONG_NAME_KEY = "ADD_SONG";
    EditText songNameEt;
    Button addSongBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_song);
        songNameEt = findViewById(R.id.songNameEt);
        addSongBtn = findViewById(R.id.addSongBtn);
        addSongBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //1. Grab the value from the edit txt
                String songNameTxt = songNameEt.getText().toString();

                //2. Make a new intent
                Intent resultIntent = new Intent();
                resultIntent.putExtra(SONG_NAME_KEY, songNameTxt);

                //2b. Set the result
                setResult(RESULT_OK, resultIntent);

                //3. Finish the activity
                finish();
            }
        });
    }
}
