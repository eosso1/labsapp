package edu.towson.cosc431.labsapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements IController, View.OnClickListener {

    private static final int ADD_SONG_CODE = 10;
    private static final String TAG = "ADD_SONG";
    ArrayList<Song> songsList;
    //Song currentSong;
    int currentSongIndex;

    TextView nameTv;
    TextView artistTv;
    TextView trackNumTv;
    Button deleteBtn;
    CheckBox isAwesomeCb;
    Button prevBtn;
    Button nextBtn;
    Button addSongBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //
        getIntent();

        setContentView(R.layout.activity_main2);
        nameTv = findViewById(R.id.songName);
        artistTv = findViewById(R.id.songArtist);
        trackNumTv = findViewById(R.id.songTrackNum);
        nextBtn = findViewById(R.id.buttonNext);
        nextBtn.setOnClickListener(this);
        addSongBtn = findViewById(R.id.addSongBtn);
        addSongBtn.setOnClickListener(this);
        // we still need references to the Buttons and Checkbox

        songsList = new ArrayList<>();
        currentSongIndex = 0;

        // put this in a helper method!
        Song song = new Song("Johnny B Good", "Chuck Berry",
                1);
        songsList.add(song);
        song = new Song("Uptown girl", "Billie Joel", 5);
        songsList.add(song);
        displayCurrentSong();
    }

    private void displayCurrentSong() {
        Song currentSong = songsList.get(currentSongIndex);
        nameTv.setText(currentSong.getName());
        // add the rest!
    }

    @Override
    public void nextSong() {
        if(currentSongIndex >= songsList.size() - 1) return;
        currentSongIndex++;
        displayCurrentSong();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.addSongBtn:
                //handle click!
                //EXPLICIT INTENT
                Intent intent = new Intent(this, AddSongActivity.class);
                startActivityForResult(intent, ADD_SONG_CODE);
                break;
            case R.id.buttonNext:
                nextSong();
                break;
            // add the rest of the click events!
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "In activity result");
        if(requestCode == ADD_SONG_CODE) {
            if(resultCode == RESULT_OK) {
                //SONG SHOULD ONLY BE HANDLED IN IF STATEMENTS
                String songNameTxt = data.getStringExtra(AddSongActivity.SONG_NAME_KEY);

                //1. Make a song object
                Song song = new Song(songNameTxt, "", -1);

                //2. Add the song to the list
                songsList.add(song);
            }
        }
    }
}
