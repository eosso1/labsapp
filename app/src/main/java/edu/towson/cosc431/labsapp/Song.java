package edu.towson.cosc431.labsapp;

/**
 * Created by randy on 2/12/18.
 */

public class Song {
    private String name;
    private String artist;
    private int trackNum;
    private boolean isAwesome;

    @Override
    public String toString() {
        return "Song{" +
                "name='" + name + '\'' +
                ", artist='" + artist + '\'' +
                ", trackNum=" + trackNum +
                ", isAwesome=" + isAwesome +
                '}';
    }

    public Song(String name, String artist, int trackNum) {
        this.name = name;
        this.artist = artist;
        this.trackNum = trackNum;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public int getTrackNum() {
        return trackNum;
    }

    public void setTrackNum(int trackNum) {
        this.trackNum = trackNum;
    }

    public boolean isAwesome() {
        return isAwesome;
    }

    public void setAwesome(boolean awesome) {
        isAwesome = awesome;
    }
}
